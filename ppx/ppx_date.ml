open Ppxlib

let date =
  let days = [| "Sun"; "Mon"; "Tue"; "Wed"; "Thu"; "Fri"; "Sat" |] in
  let months =
    [|
      "Jan";
      "Feb";
      "Mar";
      "Apr";
      "May";
      "Jun";
      "Jul";
      "Aug";
      "Sept";
      "Oct";
      "Nov";
      "Dec";
    |]
  in
  let tm = Unix.gmtime (Unix.gettimeofday ()) in
  let day, month = (days.(tm.tm_wday), months.(tm.tm_mon)) in
  Format.sprintf "%s, %02d %s %04d %02d:%02d:%02d UTC" day tm.tm_mday month
    (1900 + tm.tm_year) tm.tm_hour tm.tm_min tm.tm_sec

let expand ~loc ~path:_ = Ast_builder.Default.estring ~loc date

let extension =
  Extension.declare "date" Extension.Context.Expression
    (* Ast_pattern.(single_expr_payload (estring __)) *)
    Ast_pattern.(pstr nil)
    expand

let rule = Context_free.Rule.extension extension
let () = Driver.register_transformation ~rules:[ rule ] "date"
