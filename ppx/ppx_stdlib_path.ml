open Ppxlib

let expand ~loc ~path:_ name =
  let path =
    try Sys.getenv name
    with Not_found ->
      let wd = Unix.getcwd () in
      (* FIXME: dune working directory does not work with "../../" *)
      Filename.concat (Filename.dirname (Filename.dirname wd)) "lib"
  in
  Ast_builder.Default.estring ~loc path

let extension =
  Extension.declare "stdlib_path" Extension.Context.Expression
    Ast_pattern.(single_expr_payload (estring __))
    expand

let rule = Context_free.Rule.extension extension
let () = Driver.register_transformation ~rules:[ rule ] "stdlib_path"
